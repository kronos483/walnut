import os
import base64
from textwrap import wrap


directory = os.fsencode("svg")
dictionary = {}
max_string_length = 2048

array_name = "unicode_svgs"
    
for file in os.listdir(directory):
     filename = os.fsdecode(file)
     if filename.endswith(".svg"):
        unicode = filename.rsplit(".", 1)[0]
        code_points = unicode.split("-")

        # Modifiers are not supported for the moment
        if len(code_points) > 2:
            continue

        code_point = int(''.join(code_points), 16)
        # Max char for UTF-32 is 0x10FFFF
        if (code_point > 0x10FFFF):
            continue

        with open("svg/" + filename) as f:
            lines = str(f.read())
            lines = lines.replace('\n', ' ')
            dictionary[code_point] = lines

output_name = "Emoji_svg"

# Header file
# header = open(output_name+".h","w+")
# header.write("constexpr const mapbox::eternal::map<uint32_t, mapbox::eternal::string> " + array_name +";\n")

# Source file
cpp = open(output_name+".cpp","w+")
cpp.write("#pragma once\n")
cpp.write("#include \"eternal.hpp\"\n\n")

cpp.write("constexpr const uint32_t unicodes[] = {\n    ")
for key in sorted(dictionary):
    cpp.write(hex(key) + "ul, \n")
cpp.write("\n};\n\n")

cpp.write("constexpr const auto " + array_name+ " =  mapbox::eternal::map<uint32_t, mapbox::eternal::string>({\n")
for key in sorted(dictionary):
    char_array = dictionary[key].encode()
    base64_data = base64.b64encode(char_array).decode()
    strings = wrap(base64_data, max_string_length)
    cpp.write("   { " + hex(key) + "ul, \n");
    for s in strings:
        cpp.write("\"" + s + "\"\n");
    cpp.write(" },\n");
cpp.write("});\n")