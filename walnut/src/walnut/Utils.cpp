#include "Utils.h"
#include <cstdint>

using namespace ImGui;

    
bool ImGui::BufferingBar(const char* label, float value,  const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col) {
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;
    
    value = ImClamp(value, 0.0f, 1.0f);

    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);

    ImVec2 pos = window->DC.CursorPos;
    ImVec2 size = size_arg;
    size.x -= style.FramePadding.x * 2;
    
    const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
    ItemSize(bb, style.FramePadding.y);
    if (!ItemAdd(bb, id))
        return false;
    
    // Render
    const float circleStart = size.x * 0.7f;
    const float circleEnd = size.x;
    const float circleWidth = circleEnd - circleStart;
    
    window->DrawList->AddRectFilled(bb.Min, ImVec2(pos.x + circleStart, bb.Max.y), bg_col, style.FrameRounding);
    window->DrawList->AddRectFilled(bb.Min, ImVec2(pos.x + circleStart*value, bb.Max.y), fg_col, style.FrameRounding);
    
    const float t = g.Time;
    const float r = size.y / 2;
    const float speed = 1.5f;
    
    const float a = speed*0;
    const float b = speed*0.333f;
    const float c = speed*0.666f;
    
    const float o1 = (circleWidth+r) * (t+a - speed * (int)((t+a) / speed)) / speed;
    const float o2 = (circleWidth+r) * (t+b - speed * (int)((t+b) / speed)) / speed;
    const float o3 = (circleWidth+r) * (t+c - speed * (int)((t+c) / speed)) / speed;

    window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o1, bb.Min.y + r), r, fg_col);
    window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o2, bb.Min.y + r), r, fg_col);
    window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o3, bb.Min.y + r), r, fg_col);

    return true;
}

bool ImGui::BufferingBar(const char* label, float value, const ImVec2& size_arg) {
    return BufferingBar(label, value, size_arg, GetColorU32(ImGuiCol_FrameBg), GetColorU32(ImGuiCol_PlotHistogram));
}

bool ImGui::Spinner(const char* label, float radius, int thickness, const ImU32& color) {
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;
    
    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    
    ImVec2 pos = window->DC.CursorPos;
    ImVec2 size((radius )*2, (radius + style.FramePadding.y)*2);
    
    const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
    ItemSize(bb, style.FramePadding.y);
    if (!ItemAdd(bb, id))
        return false;
    
    // Render
    window->DrawList->PathClear();
    
    int num_segments = 30;
    int start = fabs(ImSin(g.Time*1.8f)*(num_segments-5));
    
    const float a_min = IM_PI*2.0f * ((float)start) / (float)num_segments;
    const float a_max = IM_PI*2.0f * ((float)num_segments-3) / (float)num_segments;

    const ImVec2 centre = ImVec2(pos.x+radius, pos.y+radius+style.FramePadding.y);
    
    for (int i = 0; i < num_segments; i++) {
        const float a = a_min + ((float)i / (float)num_segments) * (a_max - a_min);
        window->DrawList->PathLineTo(ImVec2(centre.x + ImCos(a+g.Time*8) * radius,
                                            centre.y + ImSin(a+g.Time*8) * radius));
    }

    window->DrawList->PathStroke(color, false, thickness);

    return true;
}

bool ImGui::Spinner(const char* label, float radius, int thickness) {
    return Spinner(label, radius, thickness, GetColorU32(ImGuiCol_PlotHistogram));
}

void ImGui::ToggleSlider(const char* str_id, bool* v, const ImU32& enabled_color) {

    ImVec2 _p = ImGui::GetCursorScreenPos();
    ImDrawList* draw_list = ImGui::GetWindowDrawList();

    ImGuiContext& g = *GImGui;
    auto style = g.Style;

    const float border_spacing = style.FrameBorderSize;
    float total_height = border_spacing + ImGui::GetFrameHeight();
    float total_width = border_spacing + total_height * 1.55f;

    ImVec2 frame{ _p.x + style.FrameBorderSize, _p.y + style.FrameBorderSize };
    ImVec2 border{ _p.x + total_width, _p.y + total_height };

    ImGui::InvisibleButton(str_id, ImVec2(total_width, total_height));
    const auto id = g.CurrentWindow->GetID(str_id);

    if (g.ActiveId == id || g.LastActiveId == id) {
        PushStyleColor(ImGuiCol_Border, GetStyleColorVec4(ImGuiCol_NavHighlight));
    }

    float height = total_height - style.FrameBorderSize - border_spacing;
    float width = total_width - style.FrameBorderSize - border_spacing;
    float radius = height * 0.50f;

    bool active_and_key_pressed = ImGui::IsKeyPressed(ImGuiKey_Space) && g.NavActivateId == id;
    if (ImGui::IsItemClicked() || active_and_key_pressed)
        *v = !*v;

    float t = *v ? 1.0f : 0.0f;

    const float ANIM_SPEED = 0.08f;
    if (g.LastActiveId == id && g.LastActiveIdTimer < ANIM_SPEED)
    {
        float t_anim = ImSaturate(g.LastActiveIdTimer / ANIM_SPEED);
        t = *v ? (t_anim) : (1.0f - t_anim);
    }

    ImU32 col_bg;
    const ImVec4 enabled_color_v = ColorConvertU32ToFloat4(enabled_color);
    const ImVec4 enabled_color_hovered = ColorConvertU32ToFloat4(GetColorU32(ImGuiCol_NavWindowingDimBg));
    if (ImGui::IsItemHovered())
        col_bg = ImGui::GetColorU32(ImLerp(ImVec4(0.78f, 0.78f, 0.78f, 1.0f), enabled_color_hovered, t));
    else
        col_bg = ImGui::GetColorU32(ImLerp(ImVec4(0.85f, 0.85f, 0.85f, 1.0f), enabled_color_v, t));

    const auto circle_color = GetColorU32(ImGuiCol_ButtonHovered);

    constexpr int circle_segments = 20;

    // draw_list->AddRect(_p, ImVec2(_p.x + width, _p.y + height), GetColorU32(ImGuiCol_Border), height * 0.5f, 0, style.FrameBorderSize);
    draw_list->AddRectFilled(_p, border, GetColorU32(ImGuiCol_Border), total_height * 0.5f);
    draw_list->AddRectFilled(frame, ImVec2(frame.x + width, frame.y + height), col_bg, height * 0.5f);
    draw_list->AddCircleFilled(ImVec2(frame.x + radius + t * (width - radius * 2.0f), frame.y + radius), radius - 1.5f, circle_color, circle_segments);


    // Pop border color if active
    if (g.ActiveId == id || g.LastActiveId == id) {
        PopStyleColor(1);
    }
}

void ImGui::FlowLayout(ImVec2 nextItemSize) {
    ImGuiStyle& style = ImGui::GetStyle();
    float window_visible_x2 = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;

    float last_button_x2 = ImGui::GetItemRectMax().x;
    float next_button_x2 = last_button_x2 + style.ItemSpacing.x + nextItemSize.x; // Expected position if next button was on same line
    if (next_button_x2 < window_visible_x2)
        ImGui::SameLine();
}

bool ImGui::ActiveButton(const char* label, const ImVec2& size_arg) {
    PushStyleVar(ImGuiStyleVar_FrameRounding, 50.0f);
    PushStyleVar(ImGuiStyleVar_FramePadding, { 8.0f , 2.0f });
    PushStyleColor(ImGuiCol_Text, IM_COL32_WHITE);
    PushStyleColor(ImGuiCol_Button, (ImU32) ImColor(38, 128, 235));
    PushStyleColor(ImGuiCol_ButtonHovered, (ImU32) ImColor(55, 142, 240));
    PushStyleColor(ImGuiCol_ButtonActive, (ImU32) ImColor(75, 156, 245));
    auto ret = Button(label, size_arg);
    PopStyleColor(4);
    PopStyleVar(2);
    SetItemDefaultFocus();
    return ret;
}

static float height = 0.0f;

bool ImGui::BeginButtonGroup(const char* label, int* select, int index, const ImVec2& size) {
    if (*select == index) {
        PushStyleColor(ImGuiCol_Text, IM_COL32_WHITE);
        PushStyleColor(ImGuiCol_Button, GetColorU32(ImGuiCol_NavHighlight));
        PushStyleColor(ImGuiCol_ButtonHovered, GetColorU32(ImGuiCol_NavWindowingDimBg));
    }
    constexpr ImDrawFlags flag = ImDrawCornerFlags_Left;
    auto ret = ButtonEx(label, size, ImGuiButtonFlags_None, flag);
    if (*select == index) {
        PopStyleColor(3);
    }

    if (ret) {
        *select = index;
    }
    height = size.y;
    ImGui::SameLine(0.0f, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);
    return ret ? ret : (*select == index);
}

bool ImGui::ButtonGroup(const char* label, int* select, int index, float width) {
    if (*select == index) {
        PushStyleColor(ImGuiCol_Text, IM_COL32_WHITE);
        PushStyleColor(ImGuiCol_Button, GetColorU32(ImGuiCol_NavHighlight));
        PushStyleColor(ImGuiCol_ButtonHovered, GetColorU32(ImGuiCol_NavWindowingDimBg));
    }
    auto ret = ImGui::Button(label, { width, height });
    if (*select == index) {
        PopStyleColor(3);
    }

    if (ret) {
        *select = index;
    }
    ImGui::SameLine(0.0f, 0.0f);
    return ret ? ret : (*select == index);
}

bool ImGui::EndButtonGroup(const char* label, int* select, int index, float width) {
    ImGui::PopStyleVar(1);
    constexpr ImDrawFlags flag = ImDrawCornerFlags_Right;
    if (*select == index) {
        PushStyleColor(ImGuiCol_Text, IM_COL32_WHITE);
        PushStyleColor(ImGuiCol_Button, GetColorU32(ImGuiCol_NavHighlight));
        PushStyleColor(ImGuiCol_ButtonHovered, GetColorU32(ImGuiCol_NavWindowingDimBg));
    }
    auto ret = ButtonEx(label, { width, height }, ImGuiButtonFlags_None, flag);
    if (*select == index) {
        PopStyleColor(3);
    }

    if (ret) {
        *select = index;
    }
    height = 0.0f;
    return ret ? ret : (*select == index);
}


void ImGui::Image(ImTextureID image, ImVec2 size, bool preserve_aspect_ratio) {
    const ImVec2 window_size = GetWindowSize();
    ImVec2 real_size;

    if (preserve_aspect_ratio) {
        const ImVec2 image_size = { size.x, size.y };

        const float image_ratio = size.x / size.y;
        const float window_ratio = window_size.x / window_size.y;

        if (window_ratio > image_ratio) {
            real_size = { image_size.x * window_size.y / image_size.y, window_size.y };
        }
        else {
            real_size = { window_size.x, image_size.y * window_size.x / image_size.x };
        }

        float left = (window_size.x - real_size.x) / 2.0f;
        float top = (window_size.y - real_size.y) / 2.0f;
        ImGui::SetCursorPos({ left , top });
    }
    else {
        real_size = window_size;
    }

    ImGui::Image(image, real_size);
}

void ImGui::ToggleButton(const char* str_id, bool* v, const ImVec2& size, const ImU32& enabled_color) {
    if (*v) {
        PushStyleColor(ImGuiCol_Text, IM_COL32_WHITE);
        PushStyleColor(ImGuiCol_Button, GetColorU32(ImGuiCol_NavHighlight));
        PushStyleColor(ImGuiCol_ButtonHovered, GetColorU32(ImGuiCol_NavWindowingDimBg));
    }
    auto ret = ImGui::Button(str_id, size);
    if (*v) {
        PopStyleColor(3);
    }
    if (ret) {
        *v = !*v;
    }
}