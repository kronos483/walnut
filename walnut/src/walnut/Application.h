#pragma once

#include "Layer.h"

#include <string>
#include <vector>
#include <memory>
#include <functional>

#include "imgui.h"
#include <vulkan/vulkan.h>

void check_vk_result(VkResult err, const char* file, int line);

#define VK_CHECK(x) check_vk_result(x, __FILE__, __LINE__)

struct GLFWwindow;

namespace Walnut {
	struct Image;

	struct ApplicationSpecification
	{
		std::string Name = "Walnut App";
		uint32_t Width = 1600;
		uint32_t Height = 900;
	};

	const char* VkResultToString(VkResult result);

	class Application
	{
	public:
		static Application* getMainApp();
		static void setMainApp(Application* app);

		Application(const ApplicationSpecification& applicationSpecification = ApplicationSpecification());
		~Application();

		void Run();
		void SetMenubarCallback(const std::function<void()>& menubarCallback) { m_MenubarCallback = menubarCallback; }
		
		template<typename T>
		void PushLayer()
		{
			static_assert(std::is_base_of<Layer, T>::value, "Pushed type is not subclass of Layer!");
			m_LayerStack.emplace_back(std::make_shared<T>())->OnAttach();
		}

		void PushLayer(const std::shared_ptr<Layer>& layer) { m_LayerStack.emplace_back(layer); layer->OnAttach(); }

		void Close();

		auto getLayers() const { return m_LayerStack; }
		auto getLayers() { return m_LayerStack; }

		void setReducedFpsWhenNotFocused(bool val) { unfocusedReducedFps = val; }
		[[nodiscard]] bool getReducedFpsWhenNotFocused() const { return unfocusedReducedFps; }

		static VkInstance GetInstance();
		static VkPhysicalDevice GetPhysicalDevice();
		static VkDevice GetDevice();

		static VkCommandBuffer GetCommandBuffer(bool begin);
		static void FlushCommandBuffer(VkCommandBuffer commandBuffer);

		static void SubmitResourceFree(std::function<void()>&& func);

		bool running() const { return m_Running; }
	private:
		void Init();
		void Shutdown();
	private:
		ApplicationSpecification m_Specification;
		GLFWwindow* m_WindowHandle = nullptr;
		bool m_Running = false;
		std::vector<std::shared_ptr<Layer>> m_LayerStack;
		std::function<void()> m_MenubarCallback;
		// Reduce fps to 15fps when window not focused
		bool unfocusedReducedFps = true;

		static inline Application* mainApp = nullptr;
	};

	// Implemented by CLIENT
	extern Application* CreateApplication(int argc, char** argv);
}
