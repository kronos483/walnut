#include "EntryPoint.h"

#include "Application.h"

namespace Walnut {

	int Main(int argc, char** argv)
	{
		Application* app = Walnut::CreateApplication(argc, argv);
		Application::setMainApp(app);
		app->Run();
		delete app;

		return 0;
	}

}

#if defined(_WIN32) && defined(FIPS_APP_WINDOWED)

#include <Windows.h>

int APIENTRY WinMain(HINSTANCE hInst, HINSTANCE hInstPrev, PSTR cmdline, int cmdshow)
{
	return Walnut::Main(__argc, __argv);
}

#else

int main(int argc, char** argv)
{
	return Walnut::Main(argc, argv);
}

#endif // FIPS_WINDOWS
