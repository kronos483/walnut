#include "Image.h"

#include "imgui.h"
#include "backends/imgui_impl_vulkan.h"

#include "Application.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <lunasvg.h>
#include <filesystem>
#include <memory>
#include <iostream>
#include <cstdint>

#include "aixlog.hpp"

namespace Walnut {

	namespace Utils {

		static uint32_t GetVulkanMemoryType(VkMemoryPropertyFlags properties, uint32_t type_bits)
		{
			VkPhysicalDeviceMemoryProperties prop;
			vkGetPhysicalDeviceMemoryProperties(Application::GetPhysicalDevice(), &prop);
			for (uint32_t i = 0; i < prop.memoryTypeCount; i++)
			{
				if ((prop.memoryTypes[i].propertyFlags & properties) == properties && type_bits & (1 << i))
					return i;
			}
			
			return 0xffffffff;
		}

		static uint32_t BytesPerChannel(ImageFormat format)
		{
			switch (format)
			{
				case ImageFormat::RGBA:    return 4;
				case ImageFormat::RGBA32F: return 32;
				default: 				   return 0;
			}
			return 0;
		}
		
		static VkFormat WalnutFormatToVulkanFormat(ImageFormat format)
		{
			switch (format)
			{
				case ImageFormat::RGBA:    return VK_FORMAT_R8G8B8A8_UNORM;
				case ImageFormat::RGBA32F: return VK_FORMAT_R32G32B32A32_SFLOAT;
			}
			return (VkFormat)0;
		}

	}

	void Image::setErrorTexture() {

		const char* svg = "<svg version=\"1.2\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 500 500\" width=\"500\" height=\"500\"><style>.a{fill:#ee0cca}.b{fill:#fff}</style><path  d=\"m0 0h500v500h-500z\"/><path fill-rule=\"evenodd\" class=\"a\" d=\"m0 300h100v100h-100zm0-200h100v100h-100zm200 200h100v100h-100zm0-200h100v100h-100zm200 200h100v100h-100zm0-200h100v100h-100zm-300 0v-100h100v100zm200 0v-100h100v100zm-100 100v100h-100v-100zm200 0v100h-100v-100zm-300 200v100h100v-100zm200 0v100h100v-100z\"/><path fill-rule=\"evenodd\" class=\"b\" d=\"m78.2 222v-75.8h47.9v8.6h-37.6v22.5h36.1v8.6h-36.1v27.5h38.6v8.6zm101.6-35.5q3.3 1.1 6.4 4.7 3.2 3.7 6.3 10.1l10.4 20.7h-11l-9.7-19.4q-3.7-7.7-7.3-10.2-3.5-2.4-9.6-2.4h-11.2v32h-10.2v-75.8h23.1q13 0 19.4 5.4 6.4 5.5 6.4 16.4 0 7.2-3.3 11.9-3.3 4.7-9.7 6.6zm-12.8-31.9h-12.9v26.9h12.9q7.4 0 11.2-3.4 3.8-3.4 3.8-10.1 0-6.6-3.8-10-3.8-3.4-11.2-3.4zm85.1 31.9q3.3 1.1 6.4 4.7 3.1 3.7 6.3 10.1l10.4 20.7h-11l-9.7-19.4q-3.8-7.7-7.3-10.2-3.5-2.4-9.6-2.4h-11.2v32h-10.3v-75.8h23.2q13 0 19.4 5.4 6.4 5.5 6.4 16.4 0 7.2-3.4 11.9-3.3 4.7-9.6 6.6zm-12.8-31.9h-12.9v26.9h12.9q7.4 0 11.2-3.4 3.8-3.4 3.8-10.1 0-6.6-3.8-10-3.8-3.4-11.2-3.4zm79.9-9.8q15.9 0 25.5 10.7 9.5 10.7 9.5 28.7 0 17.9-9.5 28.6-9.6 10.7-25.5 10.7-16 0-25.6-10.7-9.6-10.7-9.6-28.6 0-18 9.6-28.7 9.6-10.7 25.6-10.7zm0 8.3q-11.2 0-17.8 8.4-6.6 8.3-6.6 22.7 0 14.3 6.6 22.6 6.6 8.3 17.8 8.3 11.1 0 17.6-8.3 6.6-8.3 6.6-22.6 0-14.4-6.6-22.7-6.5-8.4-17.6-8.4zm87 33.4q3.3 1.1 6.4 4.7 3.2 3.7 6.3 10.1l10.4 20.7h-11l-9.7-19.4q-3.8-7.7-7.3-10.2-3.5-2.4-9.6-2.4h-11.2v32h-10.2v-75.8h23.1q13 0 19.4 5.4 6.4 5.5 6.4 16.4 0 7.2-3.3 11.9-3.3 4.7-9.7 6.6zm-12.8-31.9h-12.9v26.9h12.9q7.4 0 11.2-3.4 3.8-3.4 3.8-10.1 0-6.6-3.8-10-3.8-3.4-11.2-3.4z\"/><path fill-rule=\"evenodd\" class=\"b\" d=\"m32.5 306v-40.8h5.5v36.2h19.9v4.6zm41.9-31.4q6.5 0 10.3 4.3 3.7 4.3 3.7 11.8 0 7.5-3.7 11.8-3.8 4.3-10.3 4.3-6.6 0-10.4-4.3-3.7-4.3-3.7-11.8 0-7.5 3.7-11.8 3.8-4.3 10.4-4.3zm0 4.3q-4.1 0-6.4 3.2-2.4 3.1-2.4 8.6 0 5.5 2.3 8.7 2.4 3.1 6.5 3.1 4 0 6.3-3.1 2.4-3.2 2.4-8.7 0-5.4-2.4-8.6-2.3-3.2-6.3-3.2zm46.3 9.6v17.5h-5v-4.6q-1.7 2.7-4.3 4.1-2.6 1.3-6.3 1.3-4.7 0-7.5-2.6-2.8-2.7-2.8-7.1 0-5.2 3.5-7.8 3.5-2.6 10.3-2.6h7.1v-0.5q0-3.5-2.3-5.4-2.3-1.9-6.4-1.9-2.6 0-5.1 0.6-2.5 0.7-4.8 1.9v-4.6q2.7-1.1 5.3-1.6 2.6-0.6 5.1-0.6 6.6 0 9.9 3.5 3.3 3.4 3.3 10.4zm-5 2.1h-5q-6.1 0-8.5 1.4-2.3 1.4-2.3 4.8 0 2.6 1.7 4.2 1.8 1.6 4.8 1.6 4.2 0 6.7-3 2.6-3 2.6-7.9zm35.5-10.6v-16.5h5.1v42.5h-5.1v-4.6q-1.6 2.8-4 4.1-2.4 1.3-5.8 1.3-5.5 0-9-4.4-3.5-4.4-3.5-11.7 0-7.2 3.5-11.6 3.5-4.4 9-4.4 3.4 0 5.8 1.3 2.4 1.3 4 4zm-17.1 10.7q0 5.6 2.3 8.8 2.2 3.1 6.2 3.1 4 0 6.3-3.1 2.3-3.2 2.3-8.8 0-5.5-2.3-8.7-2.3-3.1-6.3-3.1-4 0-6.2 3.1-2.3 3.2-2.3 8.7zm32.5 15.3v-30.6h5.1v30.6zm0-36.2v-6.3h5v6.3zm41 17.7v18.5h-5v-18.3q0-4.3-1.7-6.5-1.7-2.2-5.1-2.2-4.1 0-6.4 2.6-2.4 2.6-2.4 7.1v17.3h-5.1v-30.6h5.1v4.8q1.8-2.8 4.2-4.2 2.5-1.3 5.7-1.3 5.3 0 8 3.2 2.7 3.3 2.7 9.6zm35.2-12.1v26.8q0 7.8-3.5 11.7-3.4 3.8-10.6 3.8-2.6 0-5-0.4-2.3-0.4-4.6-1.2v-4.9q2.3 1.2 4.4 1.7 2.2 0.6 4.4 0.6 5 0 7.4-2.6 2.5-2.6 2.5-7.8v-2.5q-1.6 2.7-4 4.1-2.4 1.3-5.8 1.3-5.7 0-9.1-4.3-3.5-4.3-3.5-11.3 0-7.2 3.5-11.4 3.4-4.3 9.1-4.3 3.4 0 5.8 1.3 2.4 1.3 4 4v-4.6zm-5 15q0-5.5-2.3-8.5-2.3-3-6.3-3-4.1 0-6.3 3-2.3 3-2.3 8.5 0 5.4 2.3 8.4 2.2 3 6.3 3 4 0 6.3-3 2.3-3 2.3-8.4zm33.4 15.6v-40.8h5.5v40.8zm40.1-24.7q1.9-3.4 4.5-5 2.7-1.6 6.2-1.6 4.8 0 7.4 3.3 2.6 3.4 2.6 9.5v18.5h-5.1v-18.3q0-4.4-1.5-6.5-1.6-2.2-4.8-2.2-3.9 0-6.2 2.6-2.2 2.6-2.2 7.1v17.3h-5.1v-18.3q0-4.4-1.5-6.5-1.6-2.2-4.8-2.2-3.9 0-6.2 2.7-2.2 2.6-2.2 7v17.3h-5.1v-30.6h5.1v4.8q1.7-2.9 4.1-4.2 2.4-1.3 5.7-1.3 3.3 0 5.7 1.7 2.3 1.6 3.4 4.9zm54.7 7.2v17.5h-5v-4.6q-1.8 2.8-4.3 4.1-2.6 1.3-6.3 1.3-4.7 0-7.5-2.6-2.8-2.7-2.8-7.1 0-5.2 3.5-7.8 3.4-2.6 10.3-2.6h7.1v-0.5q0-3.5-2.3-5.4-2.3-1.9-6.4-1.9-2.7 0-5.1 0.7-2.5 0.6-4.8 1.8v-4.6q2.7-1.1 5.3-1.6 2.6-0.5 5.1-0.5 6.6 0 9.9 3.4 3.3 3.4 3.3 10.4zm-5 2.1h-5q-6.1 0-8.5 1.4-2.3 1.4-2.3 4.8 0 2.7 1.7 4.2 1.8 1.6 4.8 1.6 4.2 0 6.7-2.9 2.6-3 2.6-8zm40.5-15.2v26.8q0 7.9-3.4 11.7-3.5 3.8-10.7 3.8-2.6 0-5-0.4-2.3-0.4-4.5-1.2v-4.9q2.2 1.2 4.3 1.8 2.2 0.5 4.4 0.5 5 0 7.4-2.5 2.5-2.6 2.5-7.9v-2.4q-1.6 2.7-4 4-2.4 1.3-5.8 1.3-5.6 0-9.1-4.3-3.4-4.2-3.4-11.3 0-7.1 3.4-11.4 3.5-4.3 9.1-4.3 3.4 0 5.8 1.3 2.4 1.4 4 4.1v-4.7zm-5 15q0-5.5-2.3-8.5-2.2-3-6.3-3-4.1 0-6.3 3-2.3 3-2.3 8.5 0 5.4 2.3 8.4 2.2 3 6.3 3 4.1 0 6.3-3 2.3-3 2.3-8.4zm41.6-0.9v2.4h-23.2q0.4 5.2 3.2 8 2.8 2.7 7.8 2.7 2.9 0 5.6-0.7 2.7-0.8 5.4-2.2v4.8q-2.7 1.1-5.6 1.7-2.8 0.6-5.7 0.6-7.4 0-11.7-4.2-4.2-4.3-4.2-11.6 0-7.5 4-11.9 4.1-4.4 11-4.4 6.2 0 9.8 4 3.6 3.9 3.6 10.8zm-23-1.5h17.9q0-4.1-2.3-6.6-2.2-2.5-6-2.5-4.2 0-6.7 2.4-2.5 2.4-2.9 6.7z\"/></svg>";
		auto doc = lunasvg::Document::loadFromData(svg);
		auto bitmap = doc->renderToBitmap();
		bitmap.convertToRGBA();
		auto data = bitmap.data();
		auto size = bitmap.width() * bitmap.height() * 4;
		m_Format = ImageFormat::RGBA;

		m_Width = bitmap.width();
		m_Height = bitmap.height();

		AllocateMemory(size);
		SetData(data);
	}

	Image::Image(std::string_view path)
		: m_Filepath(path)
	{
		int width, height, channels;
		uint8_t* data = nullptr;
		uint64_t size = 0;

		std::filesystem::path f(path);
		if (f.extension() == ".svg") {
			auto doc = lunasvg::Document::loadFromFile(f.string());
			if (doc == nullptr) {
				LOG(ERROR) << "Error loading image " << path << '\n';
				setErrorTexture();
			}
			else {
				auto bitmap = doc->renderToBitmap();
				bitmap.convertToRGBA();
				data = bitmap.data();
				size = bitmap.width() * bitmap.height() * 4;
				m_Format = ImageFormat::RGBA;

				m_Width = bitmap.width();
				m_Height = bitmap.height();

				AllocateMemory(size);
				SetData(data);
			}
		}
		else {
			if (stbi_is_hdr(m_Filepath.c_str()))
			{
				data = (uint8_t*)stbi_loadf(m_Filepath.c_str(), &width, &height, &channels, 4);
				size = width * height * 4 * sizeof(float);
				m_Format = ImageFormat::RGBA32F;
			}
			else
			{
				data = stbi_load(m_Filepath.c_str(), &width, &height, &channels, 4);
				size = width * height * 4;
				m_Format = ImageFormat::RGBA;
			}

			if (data == nullptr) {
				LOG(ERROR) << "Error loading image " << path << '\n';
				setErrorTexture();
			}
			else {
				m_Width = width;
				m_Height = height;

				AllocateMemory(size);
				SetData(data);

				stbi_image_free(data);
			}
		}
	}

	Image::Image(uint32_t width, uint32_t height, ImageFormat format, const void* data)
		: m_Width(width), m_Height(height), m_Format(format)
	{
		AllocateMemory(m_Width * m_Height * Utils::BytesPerChannel(m_Format));
		if (data)
			SetData(data);
	}

	Image::~Image()
	{
		Application::SubmitResourceFree([sampler = m_Sampler, imageView = m_ImageView, image = m_Image,
			memory = m_Memory, stagingBuffer = m_StagingBuffer, stagingBufferMemory = m_StagingBufferMemory]()
		{
			VkDevice device = Application::GetDevice();

			vkDestroySampler(device, sampler, nullptr);
			vkDestroyImageView(device, imageView, nullptr);
			vkDestroyImage(device, image, nullptr);
			vkFreeMemory(device, memory, nullptr);
			vkDestroyBuffer(device, stagingBuffer, nullptr);
			vkFreeMemory(device, stagingBufferMemory, nullptr);
		});
	}

	void Image::AllocateMemory(uint64_t size)
	{
		VkDevice device = Application::GetDevice();

		VkFormat vulkanFormat = Utils::WalnutFormatToVulkanFormat(m_Format);

		// Create the Image
		{
			VkImageCreateInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			info.imageType = VK_IMAGE_TYPE_2D;
			info.format = vulkanFormat;
			info.extent.width = m_Width;
			info.extent.height = m_Height;
			info.extent.depth = 1;
			info.mipLevels = 1;
			info.arrayLayers = 1;
			info.samples = VK_SAMPLE_COUNT_1_BIT;
			info.tiling = VK_IMAGE_TILING_OPTIMAL;
			info.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
			info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			VK_CHECK(vkCreateImage(device, &info, nullptr, &m_Image));
			VkMemoryRequirements req;
			vkGetImageMemoryRequirements(device, m_Image, &req);
			VkMemoryAllocateInfo alloc_info = {};
			alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			alloc_info.allocationSize = req.size;
			alloc_info.memoryTypeIndex = Utils::GetVulkanMemoryType(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, req.memoryTypeBits);
			VK_CHECK(vkAllocateMemory(device, &alloc_info, nullptr, &m_Memory));
			VK_CHECK(vkBindImageMemory(device, m_Image, m_Memory, 0));
		}

		// Create the Image View:
		{
			VkImageViewCreateInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			info.image = m_Image;
			info.viewType = VK_IMAGE_VIEW_TYPE_2D;
			info.format = vulkanFormat;
			info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			info.subresourceRange.levelCount = 1;
			info.subresourceRange.layerCount = 1;
			VK_CHECK(vkCreateImageView(device, &info, nullptr, &m_ImageView));
		}

		// Create sampler:
		{
			VkSamplerCreateInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
			info.magFilter = VK_FILTER_LINEAR;
			info.minFilter = VK_FILTER_LINEAR;
			info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
			info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
			info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
			info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
			info.minLod = -1000;
			info.maxLod = 1000;
			info.maxAnisotropy = 1.0f;
			VK_CHECK(vkCreateSampler(device, &info, nullptr, &m_Sampler));
		}

		// Create the Descriptor Set:
		m_DescriptorSet = (VkDescriptorSet)ImGui_ImplVulkan_AddTexture(m_Sampler, m_ImageView, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
	}

	void Image::SetData(const void* data)
	{
		VkDevice device = Application::GetDevice();

		size_t upload_size = m_Width * m_Height * Utils::BytesPerChannel(m_Format);

		if (!m_StagingBuffer)
		{
			// Create the Upload Buffer
			{
				VkBufferCreateInfo buffer_info = {};
				buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
				buffer_info.size = upload_size;
				buffer_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
				buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
				VK_CHECK(vkCreateBuffer(device, &buffer_info, nullptr, &m_StagingBuffer));
				VkMemoryRequirements req;
				vkGetBufferMemoryRequirements(device, m_StagingBuffer, &req);
				m_AlignedSize = req.size;
				VkMemoryAllocateInfo alloc_info = {};
				alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
				alloc_info.allocationSize = req.size;
				alloc_info.memoryTypeIndex = Utils::GetVulkanMemoryType(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, req.memoryTypeBits);
				VK_CHECK(vkAllocateMemory(device, &alloc_info, nullptr, &m_StagingBufferMemory));
				VK_CHECK(vkBindBufferMemory(device, m_StagingBuffer, m_StagingBufferMemory, 0));
			}
		}

		// Upload to Buffer
		{
			char* map = NULL;
			VK_CHECK(vkMapMemory(device, m_StagingBufferMemory, 0, m_AlignedSize, 0, (void**)(&map)));
			memcpy(map, data, upload_size);
			VkMappedMemoryRange range[1] = {};
			range[0].sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
			range[0].memory = m_StagingBufferMemory;
			range[0].size = m_AlignedSize;
			VK_CHECK(vkFlushMappedMemoryRanges(device, 1, range));
			vkUnmapMemory(device, m_StagingBufferMemory);
		}


		// Copy to Image
		{
			VkCommandBuffer command_buffer = Application::GetCommandBuffer(true);

			VkImageMemoryBarrier copy_barrier = {};
			copy_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			copy_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			copy_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			copy_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			copy_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			copy_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			copy_barrier.image = m_Image;
			copy_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			copy_barrier.subresourceRange.levelCount = 1;
			copy_barrier.subresourceRange.layerCount = 1;
			vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1, &copy_barrier);

			VkBufferImageCopy region = {};
			region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			region.imageSubresource.layerCount = 1;
			region.imageExtent.width = m_Width;
			region.imageExtent.height = m_Height;
			region.imageExtent.depth = 1;
			vkCmdCopyBufferToImage(command_buffer, m_StagingBuffer, m_Image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

			VkImageMemoryBarrier use_barrier = {};
			use_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			use_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			use_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
			use_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			use_barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			use_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			use_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			use_barrier.image = m_Image;
			use_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			use_barrier.subresourceRange.levelCount = 1;
			use_barrier.subresourceRange.layerCount = 1;
			vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, NULL, 0, NULL, 1, &use_barrier);

			Application::FlushCommandBuffer(command_buffer);
		}
	}

	static Image createImageFromDocument(lunasvg::Document* doc, uint32_t width = 0, uint32_t height = 0) {
		auto bitmap = doc->renderToBitmap(width, height);
		bitmap.convertToRGBA();

		return Image(bitmap.width(), bitmap.height(), ImageFormat::RGBA, bitmap.data());
	}

	Image Image::fromSvg(std::string_view svgData, uint32_t width, uint32_t height) {
		auto document = lunasvg::Document::loadFromData(svgData.data(), svgData.size());
		return createImageFromDocument(document.get(), width, height);
	}

	Image Image::fromSvgFile(const std::string& filename, uint32_t width, uint32_t height) {
		auto document = lunasvg::Document::loadFromFile(filename);
		return createImageFromDocument(document.get(), width, height);
	}
}


void ImGui::Image(const Walnut::Image& image, bool preserve_aspect_ratio) {
	const ImVec2 window_size = GetWindowSize();
	ImVec2 real_size;

	if (preserve_aspect_ratio) {
		const ImVec2 image_size = { (float)image.GetWidth(), (float)image.GetHeight() };

		const float image_ratio = image.GetWidth() / image.GetHeight();
		const float window_ratio = window_size.x / window_size.y;

		if (window_ratio > image_ratio) {
			real_size = { image_size.x * window_size.y / image_size.y, window_size.y };
		}
		else {
			real_size = { window_size.x, image_size.y * window_size.x / image_size.x };
		}

		float left = (window_size.x - real_size.x) / 2.0f;
		float top = (window_size.y - real_size.y) / 2.0f;
		ImGui::SetCursorPos({ left , top });
	}
	else {
		real_size = window_size;
	}

	ImGui::Image(image.GetDescriptorSet(), real_size);
}
