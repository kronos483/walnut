#include "imgui.h"
#include "imgui_internal.h"

namespace ImGui {
    bool BufferingBar(const char* label, float value, const ImVec2& size_arg);
    bool BufferingBar(const char* label, float value,  const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col);
    bool Spinner(const char* label, float radius, int thickness);
    bool Spinner(const char* label, float radius, int thickness, const ImU32& color);
    void ToggleSlider(const char* str_id, bool* v, const ImU32& enabled_color = GetColorU32(ImGuiCol_NavHighlight));
    void ToggleButton(const char* str_id, bool* v, const ImVec2& size = { 0,0 }, const ImU32& enabled_color = GetColorU32(ImGuiCol_NavHighlight));
    void FlowLayout(ImVec2 nextItemSize);

    bool ActiveButton(const char* label, const ImVec2& size_arg = ImVec2(0, 0));

    bool BeginButtonGroup(const char* label, int* v, int v_button, const ImVec2& size = ImVec2(0, 0));
    bool EndButtonGroup(const char* label, int* v, int v_button, float width = 0.0f);
    bool ButtonGroup(const char* label, int* v, int v_button, float width = 0.0f);
	void Image(ImTextureID image, ImVec2 size, bool preserve_aspect_ratio);
}