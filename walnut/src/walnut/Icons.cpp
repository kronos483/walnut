#include "Icons.h"

#include <lunasvg.h>
#include "ImGui/Emoji_svg.cpp"
#include "ImGui/Base64.h"
#include <string_view>
#include <vector>
#include <cstring>

static void RenderSvg(std::string_view svg, unsigned char* tex_pixels, ImFontAtlasCustomRect* rect, unsigned int stride) {
	auto doc = lunasvg::Document::loadFromData(svg.data(), svg.size());
	auto bitmap = doc->renderToBitmap(rect->Width, rect->Height, IM_COL32(0, 0, 0, 255));
	bitmap.convertToRGBA();

	// Fill the custom rectangle with red pixels (in reality you would draw/copy your bitmap data here!)
	for (int y = 0; y < rect->Height; y++) {
		ImU32* p = (ImU32*)tex_pixels + (rect->Y + y) * stride + (rect->X);
		auto* bitmap_data = bitmap.data() + (bitmap.width() * y * 4);
		std::memcpy(p, bitmap_data, rect->Width*4);
	}
}

bool Walnut::initIcons(ImFont* dstFont, float fsize) {
	auto& io = ImGui::GetIO();
	io.Fonts->TexPixelsUseColors = true;

	std::vector<int> indexes;
	indexes.reserve(unicode_svgs.size());
	const ImFontConfig* cfg = dstFont->ConfigData;
	if (cfg == nullptr) {
		for (auto& it : io.Fonts->ConfigData) {
			if (it.DstFont == dstFont) {
				cfg = &it;
			}
		}
	}

	//auto size = (cfg == nullptr) ? fsize : cfg->OversampleH * fsize;
	auto size = fsize;

	for (auto& unicode : unicodes) {
		bool colored = true;
		if (unicode == 0x2318ul) {
			colored = false; 
		}
		auto index = io.Fonts->AddCustomRectFontGlyph(dstFont, unicode, size, size, size + 1, {0,0}, colored);
		indexes.push_back(index);
	}

	io.Fonts->Build();

	// Retrieve texture in RGBA format
	unsigned char* tex_pixels = NULL;
	int tex_width, tex_height;
	io.Fonts->GetTexDataAsRGBA32(&tex_pixels, &tex_width, &tex_height);

	int i = 0;
	int isize = size;
	for (auto& it : unicode_svgs) {
		auto& svg = it.second;
		auto rect = io.Fonts->GetCustomRectByIndex(indexes[i]);

		auto decodedSvg = b64decode(svg.data(), strlen(svg.data()));
		RenderSvg(decodedSvg, tex_pixels, rect, tex_width);
		i++;
	}
	return true;
}
