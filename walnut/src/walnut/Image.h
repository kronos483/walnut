#pragma once

#include <string>
#include <memory>

#include "vulkan/vulkan.h"


namespace Walnut {

	enum class ImageFormat
	{
		None = 0,
		RGBA,
		RGBA32F
	};

	class Image
	{
	public:
		static Image fromSvg(std::string_view svgData, uint32_t width = 0, uint32_t height = 0);
		static Image fromSvgFile(const std::string& filename, uint32_t width = 0, uint32_t height = 0);

		Image(std::string_view path);
		Image(uint32_t width, uint32_t height, ImageFormat format, const void* data = nullptr);
		~Image();

		void SetData(const void* data);

		VkDescriptorSet GetDescriptorSet() const { return m_DescriptorSet; }

		uint32_t GetWidth() const { return m_Width; }
		uint32_t GetHeight() const { return m_Height; }

		bool valid() const { return m_DescriptorSet != nullptr; }
	private:
		void AllocateMemory(uint64_t size);
		void setErrorTexture();
	private:

		uint32_t m_Width = 0, m_Height = 0;

		VkImage m_Image = nullptr;
		VkImageView m_ImageView = nullptr;
		VkDeviceMemory m_Memory = nullptr;
		VkSampler m_Sampler = nullptr;

		ImageFormat m_Format = ImageFormat::None;

		VkBuffer m_StagingBuffer = nullptr;
		VkDeviceMemory m_StagingBufferMemory = nullptr;

		size_t m_AlignedSize = 0;

		VkDescriptorSet m_DescriptorSet = nullptr;

		std::string m_Filepath;
	};

}


namespace ImGui {
	void Image(const Walnut::Image& image, bool preserve_aspect_ratio = true);
}