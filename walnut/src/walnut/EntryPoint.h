#pragma once

namespace Walnut {
	struct Application;
	extern Application* CreateApplication(int argc, char** argv);
	int Main(int argc, char** argv);
}

#if defined(_WIN32) && defined(FIPS_APP_WINDOWED)

#include <Windows.h>

int APIENTRY WinMain(HINSTANCE hInst, HINSTANCE hInstPrev, PSTR cmdline, int cmdshow);

#else

int main(int argc, char** argv);

#endif // FIPS_WINDOWS
