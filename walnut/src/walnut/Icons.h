#pragma once

#include <imgui.h>

namespace Walnut {

	bool initIcons(ImFont* dstFont, float size);
}